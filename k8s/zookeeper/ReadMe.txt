001和002是zookeeper镜像的tar包的压缩包，由于gitee有文件大小限制，所以分包压缩了一下。
使用时下载下来解压缩出完整的tar包，docker load到本地，然后docker tag一下，然后推到自己的harbor镜像库就行了。
zookeeper.yaml是k8s集群安装的应用配置文件。
创建了StatefulSet的三个副本。利用了镜像内的脚本，自动做了初始化配置，所以没有额外的cm需要设置。
关键脚本在镜像的/apache-zookeeper-3.8.4-bin/bin目录下。
start-zookeeper是启动脚本。
zookeeper-ready是健康检查脚本。
想了解具体实现的请自行看脚本学习。